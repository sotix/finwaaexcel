package de.solka.finwaa_excel.core;

import java.util.ArrayList;
import java.util.List;

public class RawInputModel {

    private List<String> operands = new ArrayList<>();
    private String operator;

    public RawInputModel(String operator) {
        this.operator = operator;
    }
}
