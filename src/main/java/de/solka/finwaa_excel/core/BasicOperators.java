package de.solka.finwaa_excel.core;

public enum BasicOperators implements Operator<Integer> {
    ADD("+"),
    SUBTRACT("-"),
    MULTIPLY("*"),
    DIVIDE("/"),
    ;

    private String stringRepresentation;

    BasicOperators(String stringRepresentation) {
        this.stringRepresentation = stringRepresentation;
    }

    @Override
    public String getStringRepresentation() {
        return stringRepresentation;
    }

    @Override
    public String apply(Integer operand1, Integer operand2) {
        double res;
        switch (this) {
            case ADD:
                res = Math.addExact(operand1, operand2);
                break;
            case SUBTRACT:
                res = Math.subtractExact(operand1, operand2);
                break;
            case MULTIPLY:
                res = Math.multiplyExact(operand1, operand2);
                break;
            case DIVIDE:
                res = (double) operand1 / (double) operand2;
                break;
            default:
                throw new IllegalArgumentException();
        }

        return String.valueOf(res);
    }

    @Override
    public Class<Integer> getOperandType() {
        return Integer.class;
    }

    @Override
    public Integer parseOperandFromString(String string) {
        return Integer.valueOf(string);
    }
}
