package de.solka.finwaa_excel.core.exception;

/**
 * base exception for everything happening inside {@link de.solka.finwaa_excel.core.ExcelCalculator}
 */
public class ExcelCalculatorException extends RuntimeException {
    public ExcelCalculatorException(String msg) {
        super(msg);
    }

    public ExcelCalculatorException(String msg, Exception e) {
        super(msg, e);
    }
}
