package de.solka.finwaa_excel;

import de.solka.finwaa_excel.core.ExcelCalculator;
import de.solka.finwaa_excel.core.excel.JExcelReaderWriterImpl;
import de.solka.finwaa_excel.settings.Settings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Window;

import java.io.File;

public class MainController {

    @FXML
    private VBox warnings;

    @FXML
    private TextField inputFile;
    @FXML
    private TextField outputFile;

    private Settings settings;
    private ExcelCalculator calculator;

    @FXML
    public void initialize() {
        settings = new Settings();
        JExcelReaderWriterImpl jExcelReaderWriter = new JExcelReaderWriterImpl();
        calculator = new ExcelCalculator(jExcelReaderWriter, jExcelReaderWriter);
    }

    /**
     * Opens a file selection popup to choose an Excel file as input.
     * The selected file is written to the {@link MainController#inputFile}-Textfield.
     */
    @FXML
    public void onActionSelectInputFile(ActionEvent event) {
        FileChooser chooser = new FileChooser();
        chooser.setSelectedExtensionFilter(new FileChooser.ExtensionFilter("Excel files", JExcelReaderWriterImpl.SUPPORTED_EXTENSIONS));
        settings.getInitialInputDirectory().ifPresent(chooser::setInitialDirectory);
        File file = chooser.showOpenDialog(this.getWindow());

        if (file != null && file.exists()) {
            inputFile.setText(file.getAbsolutePath());

            // Set output
            String outName = file.getName().replace(".xls", "_result.xls");
            String parent = file.getParentFile().getAbsolutePath();
            outputFile.setText(new File(parent, outName).getAbsolutePath());
        }
    }

    @FXML
    public void onActionSelectOutputFile(ActionEvent event) {
        // TODO
    }

    @FXML
    public void onActionStart(ActionEvent event) {
        File input = getFileFromInput(inputFile);
        if (input == null || !input.exists()) {
            // TODO show warning
            System.out.println("no input file");
            return;
        }
        File output = getFileFromInput(outputFile);
        if (output == null) {
            // TODO show warning
            System.out.println("no output file");
            return;
        }
        try {
            calculator.calculate(input, output);
        } catch (Exception e) {
            // TODO show warning
            e.printStackTrace();
        }
    }

    private File getFileFromInput(TextField textField) {
        if (textField.getText() != null) {
            return new File(textField.getText());
        }
        return null;
    }

    private Window getWindow() {
        // we could use any component here
        // inputFile is an arbitrary choice
        return inputFile.getScene().getWindow();
    }

}
