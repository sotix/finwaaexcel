package de.solka.finwaa_excel.settings;

import java.io.File;
import java.util.Optional;

public class Settings {

    /**
     * Tries to suggest a directory from where the user could want to search for a file to open.
     * <br>
     * This could be the directory of the last opened file or from a configured folder
     *
     * @return A directory from where the user might want to look for a file to open.
     */
    public Optional<File> getInitialInputDirectory() {
        // TODO impl
        return Optional.empty();
    }
}
