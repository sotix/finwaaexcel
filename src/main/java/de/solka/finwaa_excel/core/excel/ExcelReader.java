package de.solka.finwaa_excel.core.excel;

import jxl.read.biff.BiffException;

import java.io.File;
import java.io.IOException;

public interface ExcelReader {

    /**
     * Reads the first 3 fields of the first line of the provided Excel file
     *
     * @param file Excel File to read. Should not be null
     * @return Array with the length 3. Unvalidated input. Array fields might be null.
     */
    String[] parseExcelInput(File file) throws IOException, BiffException;
}
