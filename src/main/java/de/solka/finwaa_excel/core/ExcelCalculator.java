package de.solka.finwaa_excel.core;

import de.solka.finwaa_excel.core.excel.ExcelReader;
import de.solka.finwaa_excel.core.excel.ExcelWriter;
import de.solka.finwaa_excel.core.exception.ExcelCalculatorException;
import jxl.read.biff.BiffException;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * This class does the actual work of reading the excel files, performing the calculation and
 * writing the result to an excel file.
 */
public class ExcelCalculator {

    private ExcelReader reader;
    private ExcelWriter writer;

    private Operator[] supportedOperators;

    public ExcelCalculator(ExcelReader reader, ExcelWriter writer) {
        this.reader = reader;
        this.writer = writer;

        supportedOperators = BasicOperators.values();

        validateOperators();
    }

    /**
     * Read from first file, do calculation, write output to second file
     *
     * @param from Excel-file containing the input
     * @param to   Excel-file that will contain the output. Will override existing files!
     */
    public void calculate(File from, File to) {
        String[] rawInput;
        try {
            rawInput = reader.parseExcelInput(from);
        } catch (IOException | BiffException e) {
            e.printStackTrace();
            throw new ExcelCalculatorException("could not read input file");
        }

        // look for operator
        Operator operator = null;
        int opIndex = -1;
        for (int i = 0; i < rawInput.length; i++) {
            String s = rawInput[i];
            for (Operator o : supportedOperators) {
                if (o.getStringRepresentation().equals(s)) {
                    operator = o;
                    opIndex = i;
                    break;
                }
            }
        }
        if (operator == null) {
            throw new ExcelCalculatorException("no operator found");
        }

        // find operands (the ones that are not the operator)
        String[] operadsRaw = new String[2];
        int j = 0;
        for (int i = 0; i < rawInput.length; i++) {
            if (i != opIndex) {
                operadsRaw[j] = rawInput[i];
                j++;
            }
        }

        // parse operands
        // the operator is responsible for this type-safety
        Object operand1 = operator.parseOperandFromString(operadsRaw[0]);
        Object operand2 = operator.parseOperandFromString(operadsRaw[1]);

        // calculation
        String result = operator.apply(operand1, operand2);

        try {
            writer.write(to, result);
        } catch (IOException e) {
            throw new ExcelCalculatorException("error writing result", e);
        }
    }

    private void validateOperators() {
        Set<String> reps = new HashSet<>();
        for (Operator operator : supportedOperators) {
            String toValidate = operator.getStringRepresentation();
            if (reps.contains(toValidate)) {
                throw new RuntimeException("operator representation not unique: " + toValidate);
            }
            reps.add(toValidate);
        }
    }
}
