package de.solka.finwaa_excel.core;

/**
 * Supported operators must implement this interface to be parsable from the excel file
 *
 * @param <T>
 */
public interface Operator<T> {

    /**
     * the representation in the excel file
     *
     * @return non null value. Must not interfere with other Operators !
     */
    String getStringRepresentation();

    /**
     * Applies the calculation to two operands.
     *
     * @return String representation of the result, ready to output for excel
     */
    String apply(T operand1, T operand2);

    /**
     * Type of the operands, e.g. for checking appliability
     */
    Class<T> getOperandType();

    T parseOperandFromString(String string);
}
