package de.solka.finwaa_excel;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Entry point for the program. Launches the UI.
 */
public class Main extends Application {
    public static void main(String... args) {
        launch(args);
    }

    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("main.fxml"));
        primaryStage.setTitle("Finwaa Excel Calculator");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();

    }
}
