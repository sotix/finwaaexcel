package de.solka.finwaa_excel.core.excel;

import java.io.File;
import java.io.IOException;

public interface ExcelWriter {
    /**
     * writes the String to the first cell in the Excel file. Existing files will be replaced!
     *
     * @param to      the file to which to write to
     * @param content the content for the Excel cell
     */
    void write(File to, String content) throws IOException;
}
