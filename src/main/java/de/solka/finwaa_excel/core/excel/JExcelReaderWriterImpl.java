package de.solka.finwaa_excel.core.excel;

import de.solka.finwaa_excel.core.exception.ExcelCalculatorException;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Uses the JExcel-Framework to implement {@link ExcelReader} and {@link ExcelWriter}
 */
public class JExcelReaderWriterImpl implements ExcelReader, ExcelWriter {

    public static final String[] SUPPORTED_EXTENSIONS = {"xls"};

    @Override
    public String[] parseExcelInput(File file) throws IOException, BiffException {
        Workbook workbook = Workbook.getWorkbook(file);

        // prepare output
        String[] result = new String[3];

        // if there aren't any sheets or rows in the first sheet
        // the result will stay empty
        if (workbook.getSheets().length > 0
                && workbook.getSheet(0).getRows() > 0) {
            Sheet firstSheet = workbook.getSheet(0);
            // Get first row and iterate over at most 3 cells
            // and write their content to the output-array.
            // If there aren't enough cells
            Cell[] row = firstSheet.getRow(0);
            for (int i = 0; i < row.length && i < 3; i++) {
                result[i] = row[i].getContents();
            }
        }
        return result;
    }

    @Override
    public void write(File to, String content) throws IOException {
        // delete previous file
        Files.deleteIfExists(Paths.get(to.toURI()));

        WritableWorkbook workbook = null;
        try {
            workbook = Workbook.createWorkbook(to);

            WritableSheet sheet = workbook.createSheet("Result", 0);

            sheet.addCell(new Label(0, 0, content));
        } catch (WriteException e) {
            throw new ExcelCalculatorException("error writing excel result file", e);
        } finally {
            if (workbook != null) {
                workbook.write();
                try {
                    workbook.close();
                } catch (WriteException e) {
                    e.printStackTrace();
                }
            }

        }
    }
}
