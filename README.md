# Finwaa Excel Calculator

The main function of the program is to

1. read the first line of an excel file
1. read 2 operands and an operator from that line
1. calculate the result
1. export the result in an excel file

## Questions to the client

- general
  - What is the scope? Who will use it for what?
  - Are there existing mockups/prototypes?
  - Are there plans to use the functionality in a different context?
    (scaling to multiple files, batch processing, web environment, ...)
- basic functionality
  - What Excel-Versions should be supported?
  - What operators should be supported? (simple math, boolean, advanced, ...)
  - What type of operands should be supported?
- features
  - Packaging in this format ok? (see chapter further below)
  - CLI wanted?
  - Drag and Drop of files wanted?
  - Input/Output files: should the text field be split into two fields (dir and filename)?
  - Should it be configurable, if yes, what?

# Design-Decisions

## UI-Technology: JavaFx
JavaFX was chosen because:
- existing knowledge -> reduced cost
- easy prototyping through SceneBuilder -> fast feedback
- customizable -> look and feel can be tailored for customer needs
- mature framework: well known, lots of tutorials and help available -> easy to learn for new developers
- OS-independent

Alternatives could be:
- Swing
- Qt
- non-Java frontend with Java-backend

## Excel-Support: JExcel
pros:
- simple API

cons:
- limited file format support

For a quick solution this is sufficient.

Alternatives:
- Apache POI

## Packaging: Jar-File with .bat/.sh-executable
Packaging in jar-File next to an executable script is a good balance between user-friendly interaction and fast development.
Later on there could be full executable, like an .exe-file.

# TODO
- Implement JUnit tests
- Implement real logging (e.g. slf4j)

# Feature-Ideas
- live validation of file in the text field
  example: user types a non existing file into the text input -> the input field could get a red border